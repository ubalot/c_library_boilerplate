# C library boilerplate

### Compile as static library
compile files in `/lib` as a static library with
```bash
make static
```

### Compile as shared library
compile files in `/lib` as a shared library with
```bash
make shared
```
