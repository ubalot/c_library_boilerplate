# $@ - means the target
# $^ - means all prerequisites
# $< - means just the first prerequisite

TARGET = prog

LIBRARY = projlib

BUILDDIR = build


######################## SHARED LIBRARY compilation
.PHONY: shared $(BUILDDIR)/$(TARGET)-shared $(BUILDDIR)/$(LIBRARY).so

# run project compiled with shared library
shared: $(BUILDDIR)/$(TARGET)-shared $(BUILDDIR)/lib$(LIBRARY).so
	@env LD_LIBRARY_PATH=$(BUILDDIR):${LD_LIBRARY_PATH} ./$<

# compile project with shared library
$(BUILDDIR)/$(TARGET)-shared: $(BUILDDIR)/lib$(LIBRARY).so
	@gcc -std=gnu89 src/main.c -L$(BUILDDIR) -l$(LIBRARY) -I./lib -o $@

# compile shared library
$(BUILDDIR)/lib$(LIBRARY).so: lib/$(LIBRARY).c lib/$(LIBRARY).h
	@gcc -c -fPIC $^
	@mv $(LIBRARY).o $(BUILDDIR)/$(LIBRARY).o
	@gcc -shared -o $@ $(BUILDDIR)/*.o


######################## STATIC LIBRARY compilation
.PHONY: static $(BUILDDIR)/$(TARGET)-static $(BUILDDIR)/main.o $(BUILDDIR)/$(LIBRARY).a $(BUILDDIR)/$(LIBRARY).o

# run project compiled with static library
static: $(BUILDDIR)/$(TARGET)-static $(BUILDDIR)/main.o $(BUILDDIR)/$(LIBRARY).a $(BUILDDIR)/$(LIBRARY).o
	@./$<

# link project with static library
$(BUILDDIR)/$(TARGET)-static: $(BUILDDIR)/main.o $(BUILDDIR)/$(LIBRARY).a
	@gcc $^ -o $@

# compile main project
$(BUILDDIR)/main.o: src/main.c
	@gcc -c $< -I./lib -o $@

# archive library as static
$(BUILDDIR)/$(LIBRARY).a: $(BUILDDIR)/$(LIBRARY).o
	@ar rcs $@ $^

# compile library
$(BUILDDIR)/$(LIBRARY).o: lib/$(LIBRARY).c lib/$(LIBRARY).h
	@gcc -c -o $@ $<


######################## Clean everything
.PHONY: clean clean-static clean-shared

clean: clean-static clean-shared

clean-static:
	@rm -f $(BUILDDIR)/*.o $(BUILDDIR)/*.a $(BUILDDIR)/$(TARGET)-static

clean-shared:
	@rm -f $(BUILDDIR)/*.o $(BUILDDIR)/*.so **/*.gch $(BUILDDIR)/$(TARGET)-shared